import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : 'Applications',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {
                id       : 'home',
                title    : 'Home',
                translate: 'Home',
                type     : 'item',
                icon     : 'home',
                url      : '/home',
            },
            {
                id       : 'all',
                title    : 'All',
                translate: 'All',
                type     : 'item',
                icon     : 'format_list_bulleted',
                url      : '/all',
            },
            {
                id       : 'today',
                title    : 'Today',
                translate: 'Today',
                type     : 'item',
                icon     : 'event',
                url      : '/today',
            },
            {
                id       : 'week',
                title    : 'Week',
                translate: 'Week',
                type     : 'item',
                icon     : 'event_available',
                url      : '/week',
            },
            {
                id       : 'done',
                title    : 'Done',
                translate: 'Done',
                type     : 'item',
                icon     : 'check_circle',
                url      : '/done',
            },
            {
                id       : 'deleted',
                title    : 'Deleted',
                translate: 'Deleted',
                type     : 'item',
                icon     : 'deleted',
                url      : '/deleted',
            },
            {
                id       : 'group',
                title    : 'Group',
                translate: 'Group',
                type     : 'collapsable',
                icon     : 'folder',
                children : [
                    {
                        id        : 'group-one',
                        title     : 'group-one',
                        translate: 'Group 1',
                        type      : 'item',
                        url       : '/group-one'
                    },
                    {
                        id        : 'group-two',
                        title     : 'group-two',
                        translate: 'Group 2',
                        type      : 'item',
                        url       : '/group-two'
                    },
                    {
                        id        : 'group-three',
                        title     : 'group-three',
                        translate: 'Group 3',
                        type      : 'item',
                        url       : '/group-three'
                    }
                ]
            }
        ]
    }
];