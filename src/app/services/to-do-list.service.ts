import { Injectable } from '@angular/core';
import { ITaskItems } from 'app/main/add-new-task/add-new-task.component';
import { title } from 'process';

@Injectable({
  providedIn: 'root'
})
export class ToDoListService {
  listDataSource : ITaskItems[] =[];
  dataSourceGroupOne : ITaskItems[];
  dataSourceGroupTwo : ITaskItems[];
  dataSourceGroupTree : ITaskItems[];
  constructor() { }

  getDataSourceByGroup(){
    this.dataSourceGroupOne =[];
    this.dataSourceGroupTwo =[];
    this.dataSourceGroupTree=[];
    for (let index = 0; index < this.listDataSource.length; index++) {
    if (this.listDataSource[index].group == 'Group 1') {
      this.dataSourceGroupOne.push(this.listDataSource[index]);
    } else if (this.listDataSource[index].group == 'Group 2'){
      this.dataSourceGroupTwo.push(this.listDataSource[index]);
    } else {
      this.dataSourceGroupTree.push(this.listDataSource[index]);
    }
  }
}
}
