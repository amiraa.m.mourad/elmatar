import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToDoListService } from 'app/services/to-do-list.service';

@Component({
  selector: 'app-add-new-task',
  templateUrl: './add-new-task.component.html',
  styleUrls: ['./add-new-task.component.scss']
})
export class AddNewTaskComponent implements OnInit {
  public ownerForm: FormGroup;
  public newTask: ITaskItems;
  taskTitle;
  taskDate;
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()));
  selected = 'none';
  taskDes;

  constructor(private router:Router , private _ToDoListService: ToDoListService) { }
  ngOnInit(): void {
    this.ownerForm = new FormGroup({
      title: new FormControl('', Validators.required),
      date: new FormControl(new Date()),
      group: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });
  }

// Start validation
public hasError = (controlName: string, errorName: string) =>{
  return this.ownerForm.controls[controlName].hasError(errorName);
}

public createOwner = (ownerFormValue) => {
  if (this.ownerForm.valid) {
    this.executeOwnerCreation(ownerFormValue);
  }
}
private executeOwnerCreation = (ownerFormValue) => {
  let owner : ITaskItems= {
    title: ownerFormValue.title,
    date: ownerFormValue.date,
    group: ownerFormValue.group,
    description: ownerFormValue.description
  }}
// End validation

  public addToList() {
  let items : ITaskItems[]=[];
    this.newTask = {
      title: this.taskTitle,
      date: this.taskDate,
      group: this.selected,
      description: this.taskDes
    };
    items.push(this.newTask);
    this._ToDoListService.listDataSource.push(items[0]);
    this._ToDoListService.getDataSourceByGroup();
    this.taskTitle = '';
    this.selected = '';
    this.taskDes = '';
    this.date = new FormControl(new Date());
    this.router.navigate(['']);
  }
  clearBtn(){
    this.taskTitle = '';
    this.selected = '';
    this.date = new FormControl(new Date());
    this.taskDes = '';
  }
  backBtn(){
    this.router.navigate(['']);
  }
}
export interface ITaskItems {
  title: any;
  date?: any;
  group: string;
  description?: any;
  position?: any;
  taskDone?:boolean;
}
