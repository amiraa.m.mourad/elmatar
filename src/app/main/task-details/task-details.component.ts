import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToDoListService } from 'app/services/to-do-list.service';
import { ITaskItems } from '../add-new-task/add-new-task.component';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.scss']
})
export class TaskDetailsComponent implements OnInit {
  displayedColumns: string[] = ['title', 'date', 'group', 'description'];
  dataSource: ITaskItems[];
  
  constructor(private router:Router , private dataListService: ToDoListService ) { }

  ngOnInit(): void {   
    this.dataSource = this.dataListService.listDataSource;
  }
  backBtn(){
    this.router.navigate(['']);
  }

}