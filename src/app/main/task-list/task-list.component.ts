import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToDoListService } from 'app/services/to-do-list.service';
import { ITaskItems } from '../add-new-task/add-new-task.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  displayedColumns: string[] = ['title'];
  data = Object.assign(this.dataListService.listDataSource);
  dataSource = new MatTableDataSource<ITaskItems>(this.data);
  dataSourceGroupOne = new MatTableDataSource<ITaskItems>(this.dataListService.dataSourceGroupOne);
  dataSourceGroupTwo = new MatTableDataSource<ITaskItems>(this.dataListService.dataSourceGroupTwo);
  dataSourceGroupTree = new MatTableDataSource<ITaskItems>(this.dataListService.dataSourceGroupTree);
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  selected = 'none';
  taskDone: boolean = false;

  constructor(private router: Router,
    private dataListService: ToDoListService,
    private location: Location) { }

  ngOnInit(): void {
    this.dataSource;
    this.dataSourceGroupOne;
    this.dataSourceGroupTwo;
    this.dataSourceGroupTree;
    this.dataSourceGroupOne.filterPredicate = (data:
    {title: string}, filterValue: string) =>
    data.title.trim().toLowerCase().indexOf(filterValue) !== -1;
  }
  onChange(getTitle)
  {
    /* configure filter */
      this.dataSourceGroupOne.filter = getTitle.trim().toLowerCase();
  }

  // start checkbox
  selection = new SelectionModel<ITaskItems>(true, []);
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceGroupOne.data.length;
    return numSelected === numRows;
    //  return numSelected;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceGroupOne.data.forEach(element => this.selection.select(element));
  }
  /** The label for the checkbox on the passed row */
  checkboxLabel(element?: ITaskItems): string {
    if (!element) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(element) ? 'deselect' : 'select'} element ${element.position + 1}`;
  }

  removeSelectedRows() {
    this.selection.selected.forEach(item => {
      let index: number = this.data.findIndex(d => d === item);
      this.data.splice(index, 1)
      this.dataSource = new MatTableDataSource<ITaskItems>(this.data);
      this.dataListService.getDataSourceByGroup();
      this.dataSourceGroupOne = new MatTableDataSource<ITaskItems>(this.dataListService.dataSourceGroupOne);
      this.dataSourceGroupTwo = new MatTableDataSource<ITaskItems>(this.dataListService.dataSourceGroupTwo);
      this.dataSourceGroupTree = new MatTableDataSource<ITaskItems>(this.dataListService.dataSourceGroupTree);
    });
    this.selection = new SelectionModel<ITaskItems>(true, []);
  }

  addList() {
    this.router.navigate(['AddNewTask']);
  }
  navigate(element) {
    this.router.navigate(['TaskDetails']);
    this.dataListService.listDataSource = [];
    this.dataListService.listDataSource.push(element);
  }
  delete(row_obj) {
    let index: number = this.dataListService.listDataSource.findIndex(d => d === row_obj);
    this.dataListService.listDataSource.splice(index, 1)
    this.dataSource = new MatTableDataSource<ITaskItems>(this.dataListService.listDataSource);
    this.dataListService.getDataSourceByGroup();
    this.dataSourceGroupOne = new MatTableDataSource<ITaskItems>(this.dataListService.dataSourceGroupOne);
    this.dataSourceGroupTwo = new MatTableDataSource<ITaskItems>(this.dataListService.dataSourceGroupTwo);
    this.dataSourceGroupTree = new MatTableDataSource<ITaskItems>(this.dataListService.dataSourceGroupTree);
  }
  doneSelectedRows() {
    this.selection.selected.forEach(item => {
      item.taskDone = true;
      this.taskDone = item.taskDone;
    });
    this.selection = new SelectionModel<ITaskItems>(true, []);
  }
}


